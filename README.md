# WingImporter

This Fusion 360 add-in, given one or two air foil coordinate files and some parameters, creates a wing model and generates a file of g-code to cut this wing from a polystyrene foam block.

## The cutter

The g-code generated is intended for a heated metal wire between two coordinate controllers.

![wire cutter](docs/hotcut.drawio.png)

## Install

Open https://gitlab.com/dagrende/wingimporter in a web browser. Click the download button 
![gitlab download button](docs/gitlab-download.png), choose zip and save. Unpack the zip file in your home directory, for example.
Start Fusion 360 and click the tab UTILITIES. Click the scripts and add-ins button 
![scripts and add-ins](docs/scripts-button.png), to open the dialog
![scripts dialog](docs/scripts-dialog.png)

Click the Add-ins tab, click the plus sign nd select the directory you unpacked the zip above into. Then click Run. 

Now click the tab SOLID in the main Fusion 360 window and you should see a new tool-bar button 
![wing button](docs/wing-icon.png)
If the new button is not visible, you may widen the solid design toolbar by making the Fusion 360 window wider or hiding the Data Panel to the left. 

## Example run

Open an empty Fusion 360 model, click the create toolbar button
![wing button](docs/wing-icon.png)
to open the dialog:

![dialog](docs/dialog.png)

Click OK. You will get the following wing model:

![wing model](docs/wing-model.png)

The file gcode.txt will contain:

```
; wingroot airfoil: /Users/dag/work/wingimporter/mh64.dat
; wingtip airfoil: /Users/dag/work/wingimporter/mh64.dat
; root-tip distance: 100.000
; root width: 120.000
; tip width: 100.000
; tip forward offset: 0.000
; root margin: 50.000
; tip margin: 20.000
; start dist above wing top: 5.000
; Dist from foam top to wing top: 23.000
; dist from start to wing back: 10.000
; hot wire cut diameter: 5.000
g0 y0.000 b0.000
 x0.174 a34.174
f200
m3 s500
g4 p3
g1 y-28.000 b-28.000
g1 y-35.431 b-35.431
 x10.174 y-35.431 a44.174 b-35.431
 x10.574 y-35.433 a44.464 b-35.433
 x11.783 y-35.421 a45.345 b-35.424
 x13.805 y-35.354 a46.823 b-35.375
...
 x10.407 y-40.452 a44.300 b-40.445
 x10.000 y-40.426 a44.000 b-40.426
 x0.174 a34.174
 y0.000 b0.000
m5
```
