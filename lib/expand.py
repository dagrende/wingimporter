from . import point

EPS = 0.00000001

# Outsets the clockwise path of point.Point to the left by dist
# Return the list of outset point.Point that may be longer than the original points
# points must be iterable with for p in points, and each point have properties x, y
def expand(points, dist):
    toPoints = []
    i = 0
    for p in points:
        if i > 0:
            # we have at least 2 points
            v = p.copy().subtract(lastp)
            vout = v.copy().normalize().rotate90().scaleBy(dist)
            pout = p.copy().add(vout)
            if i == 1:
                # add first point
                toPoints.append(lastp.copy().add(vout))
            if i > 1:
                # we have at least 2 segments
                c2d = lastv.cross(v)
                if dist < 0:
                    c2d = -c2d
                if c2d > EPS:
                    # inside turn
                    xing = crossing(lastpout.copy().subtract(lastv), lastpout, pout.copy().subtract(v), pout)
                    toPoints.append(xing)
                elif c2d < -EPS:
                    # outside turn - add extra line between seg ends
                    toPoints.append(lastpout)
                    if abs(dist) > EPS:
                        toPoints.append(pout.copy().subtract(v))
                # else straight ahead - skip point
        lastp = p
        if i > 0:
            lastv = v
            lastpout = pout
        i += 1
    # add last point
    toPoints.append(pout)
    return toPoints

# returns the crossing between the lines p1 - p2 and p3 - p4
# https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
def crossing(p1, p2, p3, p4):
    print("crossing(",p1, p2, p3, p4, ")")
    d = (p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x)
    a = (p1.x * p2.y - p1.y * p2.x)
    b = (p3.x * p4.y - p3.y * p4.x)
    return point.Point(
        (a * (p3.x - p4.x) - (p1.x - p2.x) * b) / d,
        (a * (p3.y - p4.y) - (p1.y - p2.y) * b) / d)

