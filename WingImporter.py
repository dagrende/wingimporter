#Author-Dag Rende
#Description-Generate model of wing and g-code for cutting it out of a foam block

import os, sys
from collections import defaultdict

import adsk.core, adsk.fusion
import traceback

from . import utils
from . import addin
from .lib import airfoil
from .lib import point
from .lib import expand

def fieldValidator(addIn, args):
    cmd = args.firingEvent.sender

    # define attribute validation (part 4 of 5)
    for input in cmd.commandInputs:
        if input.id == 'rootAirFoil':
            if input.value and len(input.value) > 0:
                if not os.path.isfile(input.value):
                    args.areInputsValid = False
            else:
                args.areInputsValid = False
        elif input.id == 'tipAirFoil':
            if input.value and len(input.value) > 0:
                if not os.path.isfile(input.value):
                    args.areInputsValid = False

def inputChanged(addIn, args):
    pass

# create points for the foils on two planes that are rootTipDistance apart
# create a loft between the two foils to visualize the wing
def executor(addIn):
    if not addIn.design:
        raise RuntimeError('No active Fusion design')
    addIn.log('execute', addIn.attributes)

    # Create a new sketch on the xy plane.
    sketches = addIn.rootComp.sketches
    yzPlane = addIn.rootComp.yZConstructionPlane

    xOffset1, xOffset2 = 0.0, addIn.attributes['rootTipDistance']
    rootMargin, tipMargin = -addIn.attributes['rootMargin'], addIn.attributes['tipMargin']
    if addIn.attributes['flipHorizontally']:
        xOffset1, xOffset2 = xOffset2, xOffset1
        rootMargin, tipMargin = tipMargin, rootMargin

    # ceate root profile
    wingRootAirFoil = airfoil.AirFoil(addIn.attributes['rootAirFoil'])
    wingRootSketch = sketches.add(yzPlane)
    wingRootSketch.attributes.add("WingImporter", "airFoil", "root")
    rootPoints3d = getPoints3d(wingRootAirFoil, addIn.attributes['rootWidth'], xOffset1, 0.0, addIn.attributes['cutDiameter'])
    createProfile(wingRootSketch, rootPoints3d)
    rootProfile = wingRootSketch.profiles.item(0)

    # ceate tip profile (use root airfoil if missing dat file)
    tipAirFoil = addIn.attributes['tipAirFoil']
    if not (tipAirFoil and len(tipAirFoil) > 0):
        tipAirFoil = addIn.attributes['rootAirFoil']
    wingTipAirFoil = airfoil.AirFoil(addIn.attributes['tipAirFoil'])
    wingTipSketch = sketches.add(yzPlane)
    wingTipSketch.attributes.add("WingImporter", "airFoil", "tip")
    tipPoints3d = getPoints3d(wingTipAirFoil, addIn.attributes['tipWidth'], xOffset2, addIn.attributes['tipForwardOffset'], addIn.attributes['cutDiameter'])
    createProfile(wingTipSketch, tipPoints3d)
    tipProfile = wingTipSketch.profiles.item(0)

    # create wing body using a loft
    loftFeats = addIn.rootComp.features.loftFeatures
    loftInput = loftFeats.createInput(adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
    loftSectionsObj = loftInput.loftSections
    loftSectionsObj.add(rootProfile)
    loftSectionsObj.add(tipProfile)
    loftInput.isSolid = False
    loftFeats.add(loftInput)

    # create two sketches - one rootMArgin out from the root and one tipMargin out from the tip, 
    # then project a foil on each plane by createing a line between each corresponding points of 
    # the root and tip foils and extrapolate each line out to the new planes. The foils on the 
    # new planes are the path to move each end of the hot wire while cutting 
    rootMarginSketch = sketches.add(yzPlane)
    rootMarginSketch.attributes.add("WingImporter", "airFoil", "rootMargin")
    rootMarginPoints = getExtrapolatedPoints(tipPoints3d, rootPoints3d, rootMargin)
    createProfile(rootMarginSketch, rootMarginPoints)
    rootMarginProfile = rootMarginSketch.profiles.item(0)

    tipMarginSketch = sketches.add(yzPlane)
    tipMarginSketch.attributes.add("WingImporter", "airFoil", "tipMargin")
    tipMarginPoints = getExtrapolatedPoints(rootPoints3d, tipPoints3d, tipMargin)
    createProfile(tipMarginSketch, tipMarginPoints)
    tipMarginProfile = tipMarginSketch.profiles.item(0)

    writeGCode(rootMarginPoints, tipMarginPoints, addIn.attributes['aboveFoamTop'], addIn.attributes['foamOverWingTop'], addIn.attributes['behindWingBack'])

# write gcode to file gcode.txt
# assume equal length of rootMarginPoints and tipMarginPoints
# test result with https://ncviewer.com/
def writeGCode(rootMarginPoints, tipMarginPoints, aboveFoamTop, foamOverWingTop, behindWingBack):
    rootMarginPoints = expand.expand([point.Point(p.y, p.z) for p in rootMarginPoints], addIn.attributes['cutDiameter'] / 2)
    firstRootMarginPoint = rootMarginPoints[0]
    tipMarginPoints = expand.expand([point.Point(p.y, p.z) for p in tipMarginPoints], addIn.attributes['cutDiameter'] / 2)
    firstTipMarginPoint = tipMarginPoints[0]
    # calculate z-max for rootMarginPoints and tipMarginPoints together
    coordRange = calculateCoordRange(rootMarginPoints, tipMarginPoints)
    top = coordRange['max'].y
    back = coordRange['min'].x
    try:
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gcode.txt'), 'w') as file:
            wg = lambda s: file.write(s + "\n")
            nv = lambda n, v: " {}{:.3f}".format(n, v * 10.0)
            comment = lambda s: wg("; " + s)
            vc = lambda s, v: comment("{}: {}".format(s, v))

            comment("wingroot airfoil: " + addIn.defaults['rootAirFoil'])
            comment("wingtip airfoil: " + addIn.defaults['tipAirFoil'])
            vc("root-tip distance", addIn.defaults['rootTipDistance'])
            vc("root width", addIn.defaults['rootWidth'])
            vc("tip width", addIn.defaults['tipWidth'])
            vc("tip forward offset", addIn.defaults['tipForwardOffset'])
            vc("root margin", addIn.defaults['rootMargin'])
            vc("tip margin", addIn.defaults['tipMargin'])
            vc("start dist above wing top", addIn.defaults['aboveFoamTop'])
            vc("Dist from foam top to wing top", addIn.defaults['foamOverWingTop'])
            vc("dist from start to wing back", addIn.defaults['behindWingBack'])
            vc("hot wire cut diameter", addIn.defaults['cutDiameter'])
            vc("cut speed", addIn.defaults['cutSpeed'])
            vc("Heat power percent", addIn.defaults['heatPower'])

            wg("g0" + nv("y", 0) + nv("b", 0))
            wg(nv("x", firstRootMarginPoint.x - back) + nv("a", firstTipMarginPoint.x - back))
            # emit speed converted from fusion default cm/s to mm/min
            wg(f"f{round(addIn.attributes['cutSpeed'] * 10 * 60)}")
            wg(f"m3 s{round(addIn.attributes['heatPower'] * 10)}")
            wg(f"g4 p3")
            wg("g1" + nv("y", -foamOverWingTop - aboveFoamTop) + nv("b", -foamOverWingTop - aboveFoamTop))
            wg("g1" + nv("y", firstRootMarginPoint.y - top - foamOverWingTop - aboveFoamTop) 
                    + nv("b", firstTipMarginPoint.y - top - foamOverWingTop - aboveFoamTop))
            for i in range(0, len(rootMarginPoints)):
                p = rootMarginPoints[i]
                x = p.x - back + behindWingBack
                y = p.y - top - foamOverWingTop - aboveFoamTop

                q = tipMarginPoints[i]
                a = q.x - back + behindWingBack
                b = q.y - top - foamOverWingTop - aboveFoamTop

                if addIn.attributes['flipHorizontally']:
                    x, y, a, b = a, b, x, y
                    
                wg(nv("x", x) + nv("y", y) + nv("a", a) + nv("b", b))
            wg(nv("x", firstRootMarginPoint.x - back) + nv("a", firstTipMarginPoint.x - back))
            wg(nv("y", 0) + nv("b", 0))
            wg(f"m5")

    except:
        addIn.log(traceback.format_exc())
        utils.messageBox(traceback.format_exc())

# returns foil as ObjectCollection of Point3D back-top-front-bottom-back, 
# actual position and scale, unit cm
# first point is back, points in positive direction towards front, z in positive direction towards top
def getPoints3d(airFoil, wingWidth, xOffset, forwardOffset, dist):
    points = adsk.core.ObjectCollection.create()
    for p in list(airFoil.points):
        points.add(
            adsk.core.Point3D.create(xOffset, -p.x * wingWidth + forwardOffset, p.y * wingWidth)
        )
    return points

# returns a Dict with min x,y,z point and max x,y,z point
def calculateCoordRange(*pointses):
    rangeMin = point.Point(sys.float_info.max, sys.float_info.max, sys.float_info.max)
    rangeMax = point.Point(sys.float_info.min, sys.float_info.min, sys.float_info.min)
    for points in pointses:
        for p in points:
            rangeMin.x = min(rangeMin.x, p.x)
            rangeMin.y = min(rangeMin.y, p.y)
            rangeMin.z = min(rangeMin.z, p.z)
            rangeMax.x = max(rangeMax.x, p.x)
            rangeMax.y = max(rangeMax.y, p.y)
            rangeMax.z = max(rangeMax.z, p.z)
    return {"min": rangeMin, "max": rangeMax, 
        "width": rangeMax.x - rangeMin.x, 
        "height": rangeMax.y - rangeMin.y}

# return a collection of points that are the extrapolation 
# of each fromPoint to toPoint outset by marginX
def getExtrapolatedPoints(fromPoints3d, toPoints3d, marginX):
    if not fromPoints3d.count == toPoints3d.count:
        raise RuntimeError('root and tip point count is not same - cannot create margin profiles')
    extrapolatedPoints = adsk.core.ObjectCollection.create()
    for i in range(0, fromPoints3d.count):
        direction = fromPoints3d.item(i).vectorTo(toPoints3d.item(i))
        direction.normalize()
        marginVector = adsk.core.Vector3D.create(1, 0, 0)
        directionMarginVectorDotProd = direction.dotProduct(marginVector)
        direction.scaleBy(marginX / directionMarginVectorDotProd)
        extrapolatedPoint = toPoints3d.item(i).copy()
        extrapolatedPoint.translateBy(direction)
        extrapolatedPoints.add(extrapolatedPoint)
    return extrapolatedPoints

def createProfile(sketch, points3d):
    points = adsk.core.ObjectCollection.create()
    for p in points3d:
        points.add(sketch.modelToSketchSpace(p))
    sketch.sketchCurves.sketchFittedSplines.add(points)

params = [
    addin.StringParam('rootAirFoil', '', 'Wing root airfoil', 'Airfoil dat file path or URL'),
    addin.StringParam('tipAirFoil', '', 'Wingtip airfoil', "Airfoil dat file path or URL"),
    addin.NumberParam('rootTipDistance', 'mm', '50 mm', 'Root-tip distance', "Distance from wing root to tip"),
    addin.NumberParam('rootWidth', 'mm', '100 mm', 'Root width', "Distance between wing root front and back"),
    addin.NumberParam('tipWidth', 'mm', '100 mm', 'Tip width', "Distance between wing tip front and back"),
    addin.NumberParam('tipForwardOffset', 'mm', '0 mm', 'Tip forward offset', "Distance to move tip forward"),
    addin.NumberParam('rootMargin', 'mm', '0 mm', 'Root margin', 'Distance from root to hot wire end'),
    addin.NumberParam('tipMargin', 'mm', '0 mm', 'Tip margin', 'Distance from tip to hot wire end'),
    addin.NumberParam('aboveFoamTop', 'mm', '5 mm', 'Start dist above wing top', 'Vertical distance from hot wire start to highest point on wing'),
    addin.NumberParam('foamOverWingTop', 'mm', '5 mm', 'Dist from foam top to wing top', 'Distance from top of foam block to highest part of wing'),
    addin.NumberParam('behindWingBack', 'mm', '10 mm', 'Dist from start to wing back', 'Distance from horizontal cut start to wing backend'),
    addin.NumberParam('cutDiameter', 'mm', '1 mm', 'hot wire cut diameter', 'Diameter of the cut due to wire temperature and cut speed, needed for radius compensation'),
    addin.NumberParam('cutSpeed', 'mm/min', '200 mm/min', 'cut speed', 'hot wire moving speed when cutting'),
    addin.NumberParam('heatPower', '', '50', 'heat power percent', 'Percent of full heat power 0-100'),
    addin.BoolParam('flipHorizontally', False, 'Flip wing horizontally', 'Flip left right to cut the other side wing part')
]

addIn = addin.AddIn("wingImporter", 'WingImporter', params)
addIn.description = 'Creates tabs from one body into another body in front of it. Suitable for gluing together perpendicular walls.'

addIn.executor = executor
addIn.fieldValidator = fieldValidator

def selChanged(args):
    pass
addIn.ui.activeSelectionChanged.add(addIn.handlers.make_handler(adsk.core.ActiveSelectionEventHandler, selChanged))

def run(context):
    try:
        addIn.addButton()
    except:
        utils.messageBox(traceback.format_exc())


def stop(context):
    try:
        addIn.removeButton()
    except:
        utils.messageBox(traceback.format_exc())
